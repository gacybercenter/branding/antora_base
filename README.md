# Georgia Cyber Center Antora Theme

This project is a modification to the default Antora UI theme to follow branding guidelines.

## Cascading Style Sheets (CSS)

```
src
├── css
│   ├── footer.css
│   ├── gcc.css
│   └── site.css
├── img
│   ├── GCC_Fortress only black.svg
│   ├── GCC_Fortress only white.svg
│   ├── caret.svg
│   ├── chevron.svg
│   ├── favicon.ico
│   ├── gcc_horz_4c.svg
│   ├── gcc_horz_white.svg
│   ├── gcc_vert_stacked_4c.svg
│   ├── gcc_vert_stacked_white.svg
│   └── social-icon
│       ├── LinkedIn_black.svg
│       ├── facebook_black.svg
│       ├── github_black.svg
│       ├── stackoverflow_black.svg
│       ├── twitter_black.svg
│       └── youtube_black.svg
└── partials
    └── footer-content.hbs
```